import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./modules/dashboard/dashboard.module').then(mod => mod.DashboardModule)},
  { path: 'forms', loadChildren: () => import('./modules/forms/forms.module').then(mod => mod.FormsModule)},
  { path: 'submissions', loadChildren: () => import('./modules/submissions/submissions.module').then(mod => mod.SubmissionsModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
