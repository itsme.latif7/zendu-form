import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderSubmissionPageComponent } from './header-submission-page.component';

describe('HeaderSubmissionPageComponent', () => {
  let component: HeaderSubmissionPageComponent;
  let fixture: ComponentFixture<HeaderSubmissionPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderSubmissionPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderSubmissionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
