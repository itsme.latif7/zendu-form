import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import { SubmissionsModel } from '../../models/submissions.model';
import { SubmissionsConstant } from '../../constant/submission.constant';
import { SubmissionsDummyData } from '../../test-data/submissions-dumy';

@Component({
  selector: 'app-header-submission-page',
  templateUrl: './header-submission-page.component.html',
  styleUrls: ['./header-submission-page.component.scss']
})
export class HeaderSubmissionPageComponent implements OnInit, OnChanges {

  @Input() optionGroup!: string[];
  @Input() activeOptionGroup!: string;
  @Output() outputEmit: EventEmitter<string> = new EventEmitter<string>();

  listDataDummy: SubmissionsModel[] = SubmissionsDummyData.data;

  constructor() { }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {
      const changeActiveOption = changes['activeOptionGroup'];
      if(changeActiveOption.currentValue !== changeActiveOption.firstChange){
        this.activeOptionGroup = changeActiveOption.currentValue;
      }
  }

  buttonClass(option: string){
    if(this.activeOptionGroup === option){
      return 'btn btn-active';
    }

    return 'btn btn-secondary';
  }

  onChangeOption(option: string){
    this.activeOptionGroup = option;
    this.outputEmit.emit(option);
  }

  onExport(){
    const fileType = SubmissionsConstant.fileTypeDownload;
    const fileExtension = SubmissionsConstant.fileExtensionDownload;
    const fileName = "Donwload Submissions";

    const ws = XLSX.utils.json_to_sheet(this.listDataDummy);

    let objWb = { Sheets:  {}, SheetNames: [fileName] } as any;
    objWb['Sheets'][fileName] = ws;

    const excelBuffer = XLSX.write(objWb, { bookType: "xlsx", type: "array" });
    const data = new Blob([excelBuffer], { type: fileType });

    saveAs(data, fileName + fileExtension)
  }

}
