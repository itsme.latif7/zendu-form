import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSubmissionsPageComponent } from './list-submissions-page.component';

describe('ListSubmissionsPageComponent', () => {
  let component: ListSubmissionsPageComponent;
  let fixture: ComponentFixture<ListSubmissionsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSubmissionsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSubmissionsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
