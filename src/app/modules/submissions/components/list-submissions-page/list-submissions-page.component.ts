import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SubmissionsConstant } from '../../constant/submission.constant';
import { SubmissionsModel } from '../../models/submissions.model';
import { SubmissionsService } from '../../services/submissions.service';

@Component({
  selector: 'app-list-submissions-page',
  templateUrl: './list-submissions-page.component.html',
  styleUrls: ['./list-submissions-page.component.scss']
})
export class ListSubmissionsPageComponent implements OnInit, OnDestroy {

  listSubmissions: SubmissionsModel[] = [];
  subscription: Subscription[] = [];

  constructor(
    private _submissionService: SubmissionsService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  ngOnDestroy(): void {
      this.subscription.forEach(el => el.unsubscribe());
  }

  loadData(){
    const subs = this._submissionService.loadData().subscribe(res => {
      this.listSubmissions = res;
    });

    this.subscription.push(subs);
  }

  statusActive(status: number): boolean{
    return status === SubmissionsConstant.statusActive;
  }

  labelBadge(status_id: number): string{
    switch(status_id){
      case SubmissionsConstant.statusLowRisk:
        return 'badge low-risk';
      case SubmissionsConstant.statusUnassigned:
        return 'badge uncomplete';
      case SubmissionsConstant.statusUncompleate:
        return 'badge unassigned';
      default:
        return 'badge';
    }
  }

  labelDueDate(dueDate: number): string{
    const dateNow = Math.floor(Date.now() / 1000);
    if(dueDate < dateNow){
      return 'text-danger';
    }
      return 'text-normal';
  }

}
