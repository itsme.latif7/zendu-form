import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapListSubmissionsPageComponent } from './map-list-submissions-page.component';

describe('MapListSubmissionsPageComponent', () => {
  let component: MapListSubmissionsPageComponent;
  let fixture: ComponentFixture<MapListSubmissionsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapListSubmissionsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapListSubmissionsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
