import { Component, Input, OnInit } from '@angular/core';
import { SubmissionsModel } from '../../../models/submissions.model';
import { SubmissionsService } from '../../../services/submissions.service';

@Component({
  selector: 'app-map-list-submissions-page',
  templateUrl: './map-list-submissions-page.component.html',
  styleUrls: ['./map-list-submissions-page.component.scss']
})
export class MapListSubmissionsPageComponent implements OnInit {

  @Input() listSubmissions!: SubmissionsModel[];

  constructor(
    private _submissionService: SubmissionsService
  ) { }

  ngOnInit(): void {
  }


  labelBadge(status_id: number): string{
    return this._submissionService.labelBadge(status_id);
  }

  labelDueDate(dueDate: number): string{
    return this._submissionService.labelDueDate(dueDate);
  }

}
