import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapMarkerSubmissionPageComponent } from './map-marker-submission-page.component';

describe('MapMarkerSubmissionPageComponent', () => {
  let component: MapMarkerSubmissionPageComponent;
  let fixture: ComponentFixture<MapMarkerSubmissionPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapMarkerSubmissionPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapMarkerSubmissionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
