import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import * as L from 'leaflet'
import { SubmissionsConstant } from '../../../constant/submission.constant';
import { SubmissionsModel } from '../../../models/submissions.model';

export interface MarkerModel {
  marker: any;
  icon: any;
}

@Component({
  selector: 'app-map-marker-submission-page',
  templateUrl: './map-marker-submission-page.component.html',
  styleUrls: ['./map-marker-submission-page.component.scss']
})
export class MapMarkerSubmissionPageComponent implements OnInit, AfterViewInit {
  @Input() listSubmissions: SubmissionsModel[] = [];

  private map: any;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.generateMap();
  }

  generateMap(){

    const svgIcon = L.divIcon({
      html: SubmissionsConstant.svgIcon,
      className: "",
      iconSize: [24, 40],
      iconAnchor: [12, 40],
    });

    this.map = L.map('map', {
      center: [ 51.505, -0.09 ],
      zoom: 13
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    let marker;

    for (let i = 0; i < this.listSubmissions.length; i++) {
      marker = L.marker([this.listSubmissions[i].long, this.listSubmissions[i].lat], {icon: svgIcon})
        .addTo(this.map);
    }

    tiles.addTo(this.map);

  }

}
