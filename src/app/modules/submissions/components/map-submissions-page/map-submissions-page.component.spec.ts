import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapSubmissionsPageComponent } from './map-submissions-page.component';

describe('MapSubmissionsPageComponent', () => {
  let component: MapSubmissionsPageComponent;
  let fixture: ComponentFixture<MapSubmissionsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapSubmissionsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapSubmissionsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
