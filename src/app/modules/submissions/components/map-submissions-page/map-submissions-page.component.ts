import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SubmissionsModel } from '../../models/submissions.model';
import { SubmissionsService } from '../../services/submissions.service';

@Component({
  selector: 'app-map-submissions-page',
  templateUrl: './map-submissions-page.component.html',
  styleUrls: ['./map-submissions-page.component.scss']
})
export class MapSubmissionsPageComponent implements OnInit, OnDestroy {

  listSubmissions: SubmissionsModel[] = [];
  subscription: Subscription[] = [];

  constructor(
    private _submissionService: SubmissionsService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  ngOnDestroy(): void {
      this.subscription.forEach(el => el.unsubscribe());
  }

  loadData(){
    const subs = this._submissionService.loadData().subscribe(res => {
      this.listSubmissions = res;
    });

    this.subscription.push(subs);
  }

}
