import { Component, OnInit } from '@angular/core';
import { SubmissionsConstant } from '../constant/submission.constant';
import { SubmissionsModel } from '../models/submissions.model';

@Component({
  selector: 'app-submissions-page',
  templateUrl: './submissions-page.component.html',
  styleUrls: ['./submissions-page.component.scss']
})
export class SubmissionsPageComponent implements OnInit {

  optionGroup = SubmissionsConstant.optionGroup;
  activeOptionGroup = SubmissionsConstant.actionOptionGroup;
  listSubmissions: SubmissionsModel[] = [];

  constructor(
  ) { }

  ngOnInit(): void {

  }

  get listPage(){
    return this.activeOptionGroup === SubmissionsConstant.listPage;
  }

  get mapPage(){
    return this.activeOptionGroup === SubmissionsConstant.mapPage;
  }

}
