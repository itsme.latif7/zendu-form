export interface SubmissionsModel {
  id: number;
  task: string;
  status_active: number;
  status_id: number;
  status_label: string;
  from: string;
  to: string;
  customer_address: string;
  due_date: number;
  long: number;
  lat: number;
}
