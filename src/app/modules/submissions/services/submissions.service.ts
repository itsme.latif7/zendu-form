import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { SubmissionsConstant } from '../constant/submission.constant';
import { SubmissionsModel } from '../models/submissions.model';
import { SubmissionsDummyData } from '../test-data/submissions-dumy';

@Injectable({
  providedIn: 'root'
})
export class SubmissionsService {
  private dummyData: SubmissionsModel[] = SubmissionsDummyData.data;

  constructor() { }

  loadData(): Observable<SubmissionsModel[]>{
    return of<SubmissionsModel[]>(this.dummyData);
  }

  labelBadge(status_id: number): string{
    switch(status_id){
      case SubmissionsConstant.statusLowRisk:
        return 'badge low-risk';
      case SubmissionsConstant.statusUnassigned:
        return 'badge uncomplete';
      case SubmissionsConstant.statusUncompleate:
        return 'badge unassigned';
      default:
        return 'badge';
    }
  }

  labelDueDate(dueDate: number): string{
    const dateNow = Math.floor(Date.now() / 1000);
    if(dueDate < dateNow){
      return 'text-danger';
    }
      return 'text-normal';
  }
}
