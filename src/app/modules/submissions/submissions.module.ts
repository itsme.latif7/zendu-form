import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubmissionsRoutingModule } from './submissions-routing.module';
import { SubmissionsPageComponent } from './components/submissions-page.component';
import { ListSubmissionsPageComponent } from './components/list-submissions-page/list-submissions-page.component';
import { HeaderSubmissionPageComponent } from './components/header-submission-page/header-submission-page.component';
import { MapSubmissionsPageComponent } from './components/map-submissions-page/map-submissions-page.component';
import { MapMarkerSubmissionPageComponent } from './components/map-submissions-page/map-marker-submission-page/map-marker-submission-page.component';
import { MapListSubmissionsPageComponent } from './components/map-submissions-page/map-list-submissions-page/map-list-submissions-page.component';
import { ThemeModule } from '../theme/theme.module';


@NgModule({
  declarations: [
    SubmissionsPageComponent,
    ListSubmissionsPageComponent,
    HeaderSubmissionPageComponent,
    MapSubmissionsPageComponent,
    MapMarkerSubmissionPageComponent,
    MapListSubmissionsPageComponent
  ],
  imports: [
    CommonModule,
    SubmissionsRoutingModule,
    ThemeModule
  ]
})
export class SubmissionsModule { }
