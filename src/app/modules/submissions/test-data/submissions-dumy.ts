export class SubmissionsDummyData {
  static data = [
    {
      "id": 123,
      "task": "Work Flow: Requires Location",
      "status_active": 0,
      "status_id": 1,
      "status_label": "Low Risk",
      "from": "zendu@zendu.com",
      "to": "tracy@zenduit.com",
      "customer_address": "2118 Thornridge Cir. Syracuse, Connecticut 35624",
      "due_date": 1666462200,
      "long": 51.5,
      "lat": -0.09
    },
    {
      "id": 123,
      "task": "Work Flow: Requires Location",
      "status_active": 1,
      "status_id": 1,
      "status_label": "Low Risk",
      "from": "zendu@zendu.com",
      "to": "tracy@zenduit.com",
      "customer_address": "1901 Thornridge Cir. Shiloh, Hawaii 81063",
      "due_date": 1671732600,
      "long": 51.5,
      "lat": -0.11
    },
    {
      "id": 123,
      "task": "Work Flow: Requires Location",
      "status_active": 0,
      "status_id": 2,
      "status_label": "Uncomplete",
      "from": "zendu@zendu.com",
      "to": "tracy@zenduit.com",
      "customer_address": "4140 Parker Rd. Allentown, New Mexico 31134",
      "due_date": 1671732600,
      "long": 51.5,
      "lat": -0.12
    },
    {
      "id": 123,
      "task": "Work Flow: Requires Location",
      "status_active": 0,
      "status_id": 3,
      "status_label": "Unassigned",
      "from": "zendu@zendu.com",
      "to": "tracy@zenduit.com",
      "customer_address": "4140 Parker Rd. Allentown, New Mexico 31134",
      "due_date": 1666462200,
      "long": 51.5,
      "lat": -0.14
    }
  ]
}
