import { Component, OnInit } from '@angular/core';
import { MenuModel } from '../../models/menu.model';
import { MenuService } from '../../services/menu.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  listMenu!: MenuModel[];
  constructor(
    private _menuService: MenuService
  ) { }

  ngOnInit(): void {
    this.listMenu = this._menuService.getMenu();
  }


}
