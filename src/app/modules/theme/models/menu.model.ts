export interface MenuModel{
  url: string;
  label: string;
  icon: string;
}
