import { Injectable } from '@angular/core';
import { MenuModel } from '../models/menu.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private listMenu: MenuModel[] = [
    { url: 'forms', label: 'Forms', icon: 'bi bi-list-ul' },
    { url: 'customers', label: 'Customers', icon: 'bi bi-people-fill' },
    { url: 'submissions', label: 'Submissions', icon: 'bi bi-soundwave' },
    { url: 'history', label: 'History', icon: 'bi bi-arrow-counterclockwise' },
    { url: 'reports', label: 'Reports', icon: 'bi bi-graph-up' },
    { url: 'workflow', label: 'Workflow', icon: 'bi bi-graph-up' }
  ];

  constructor() { }

  getMenu(): MenuModel[]{
    return this.listMenu;
  }
}
